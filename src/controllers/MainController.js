import NavigationView from "../views/NavigationView";
import Warehouse from "../models/Warehouse";
import Section, {gridOne, gridThree, gridTwo} from "../models/Section";
import HeadingView from "../views/HeadingView";
import Weather from "../models/Weather";
import GridView from "../views/GridView";
import SidebarView from "../views/SidebarView";
import CreateProductView from "../views/CreateProductView";
import Product from "../models/Product";
import ProductInformationView from "../views/ProductInformationView";

const useLocalStorage = true;

export default class MainController {
    constructor() {
        if (useLocalStorage && localStorage.getItem('warehouse')) {
            let data = JSON.parse(localStorage.getItem('warehouse'));
            let sections = [];
            data.sections.forEach(data => {
                let products = [];
                data.products.forEach(data => {
                    products.push(new Product(data));
                })
                data.products = products;
                let section = new Section(data);
                sections.push(section);
            })
            this.warehouse = new Warehouse({
                id: data.id,
                sections: sections,
                activeSection: null
            });
            if (data.activeSection != null) this.warehouse.changeActiveSection(data.activeSection.name);
            this.weather = new Weather(JSON.parse(localStorage.getItem('weather')));
        } else {
            this.warehouse = new Warehouse({
                id: 1,
                sections: [
                    new Section({id: 1, name: 'Kleding', grid: gridOne, products: []}),
                    new Section({id: 2, name: 'Tierlantijn', grid: gridTwo, products: []}),
                    new Section({id: 3, name: 'Decoratie', grid: gridThree, products: []}),
                ],
                activeSection: null,
            });
            this.weather = new Weather();
        }
        this.navigationView = new NavigationView(this);
        this.headingView = new HeadingView(this);
        this.gridView = new GridView(this);
        this.sidebarView = new SidebarView(this);
        this.createProductView = new CreateProductView(this);
        this.productInformationView = new ProductInformationView(this);
        this.viewNavigation(false);
        this.viewHeading();
        this.viewSection('grid');
        this.viewSidebar()
    }

    viewNavigation(createProduct) {
        this.navigationView.draw(this.warehouse.getSections(), this.warehouse.getActiveSection(), createProduct);
    }

    viewHeading() {
        if (this.warehouse.getActiveSection() != null) {
            this.headingView.draw(this.warehouse.getActiveSection().name);
        }
    }

    viewSection(view) {
        switch (view) {
            case 'grid':
                this.viewGrid();
                break;
            case 'createProduct':
                this.viewCreateProduct();
                break;
        }
    }

    viewGrid() {
        if (this.warehouse.getActiveSection() != null) {
            this.gridView.draw(this.warehouse.getActiveSection().grid, this.warehouse.getProductsFromActiveSection());
        }
    }

    viewSidebar() {
        if (this.warehouse.getActiveSection() != null) {
            this.sidebarView.draw(this.weather.locationData, this.warehouse.getNotPlacedProductsFromActiveSection());
        }
    }

    actionChangeWarehouseActiveSection(name) {
        this.warehouse.changeActiveSection(name);
        this.warehouse.saveToLocalStorage();
        this.viewNavigation(false);
        this.viewHeading();
        this.viewSection('grid');
        this.viewSidebar();
    }

    getWeatherDataByQuery(location) {
        return this.weather.getWeatherByQuery(location);
    }

    actionStoreLocationInLocalStorage(data) {
        localStorage.setItem('weather', JSON.stringify(data));
    }

    actionCreateProduct() {
        if (this.warehouse.getActiveSection() != null) {
            this.viewNavigation(true);
            this.viewSection('createProduct');
        }
    }

    viewCreateProduct() {
        this.createProductView.draw();
    }

    actionCreateNewProduct(props) {
        let product = new Product({
            name: props.name,
            description: props.description,
            purchasePrice: props.purchasePrice,
            sellingPrice: props.sellingPrice,
            minimalStock: props.minimalStock,
            currentStock: props.currentStock,
            color: props.color,
            size: props.size,
            count: props.count,
            weight: props.weight,
            placed: false,
            image: null,
        });
        this.warehouse.addProductToActiveSection(product);
        this.warehouse.saveToLocalStorage();
        this.viewNavigation(false);
        this.viewSection('grid');
        this.viewSidebar();
    }

    actionDeleteProduct(product) {
        this.warehouse.removeProductFromActiveSection(product);
        this.warehouse.saveToLocalStorage();
        this.viewSection('grid');
        this.viewSidebar();
    }

    actionRemoveProductFromGrid(product) {
        this.warehouse.removeProductFromGridInActiveSection(product);
        this.warehouse.saveToLocalStorage();
        this.viewSection('grid');
        this.viewSidebar();
    }

    actionPlaceProductOnCell(name, cord) {
        let product = this.warehouse.getProductByNameFromActiveSection(name);
        this.warehouse.placeProductInActiveSection(product, cord);
        this.warehouse.saveToLocalStorage();
        this.viewSection('grid');
        this.viewSidebar();
    }

    getWarehouseActiveSectionName() {
        return this.warehouse.getActiveSection().name;
    }

    viewProductInformation(product) {
        this.productInformationView.draw(product, this.warehouse.getActiveSection().name);
    }

    actionUploadImageToProduct(product, image) {
        this.warehouse.setProductImage(product, image);
        this.warehouse.saveToLocalStorage();
    }
}