export default class Warehouse {
    constructor(data) {
        Object.assign(this, data)
    }

    getSections() {
        return this.sections;
    }

    getSectionById(id) {
        return this.sections.find(function (section) {
            return section.id === id;
        });
    }

    getSectionByName(name) {
        return this.sections.find(function (section) {
            return section.name === name;
        });
    }

    getActiveSection() {
        if (this.activeSection != null) {
            return this.activeSection;
        } else {
            return null;
        }
    }

    changeActiveSection(name) {
        this.activeSection = this.getSectionByName(name);
    }

    addProductToActiveSection(product) {
        this.activeSection.addProduct(product);
    }

    getProductsFromActiveSection() {
        return this.activeSection.products;
    }

    setProductsToActiveSection(products) {
        this.activeSection.products = products;
    }

    getNotPlacedProductsFromActiveSection() {
        return this.activeSection.getNotPlacedProducts();
    }

    getProductByNameFromActiveSection(name) {
        return this.activeSection.getProductByName(name);
    }

    placeProductInActiveSection(product, cord) {
        this.activeSection.placeProduct(product, cord);
    }

    setProductImage(product, image) {
        product.image = image;
    }

    removeProductFromGridInActiveSection(product) {
        this.activeSection.removeProductFromGrid(product);
    }

    removeProductFromActiveSection(product) {
        let products = this.getProductsFromActiveSection();
        this.activeSection.removeProductFromGrid(product);
        let newProducts = products.filter(function (el) {
            return el.name !== product.name;
        });
        this.setProductsToActiveSection(newProducts);
    }

    saveToLocalStorage() {
        localStorage.setItem('warehouse', JSON.stringify(this));
    }
}