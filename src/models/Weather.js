const util = require('util')

export default class Weather {
    constructor(locationData) {
        if (locationData != null) {
            this.locationData = Promise.resolve(locationData);
        } else {
            this.getWeatherByQuery('Wijchen');
        }
    }

    getWeatherByQuery(location) {
        return this.locationData = fetch('http://api.openweathermap.org/data/2.5/weather?q=' + location + '&appid=62f7ba282a6bdc30f5c263bf084aefa5')
            .then(response => response.json());
    }
}