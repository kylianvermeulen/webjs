export default class CreateProductView {
    constructor(controller) {
        this.controller = controller;
        this.template = document.getElementById('sectionMain');
        this.inputs = {
            all: [
                {name: 'name', text: 'Productnaam'},
                {name: 'description', text: 'Beschrijving'},
                {name: 'purchasePrice', text: 'Inkoopprijs'},
                {name: 'sellingPrice', text: 'Verkoopprijs ex btw'},
                {name: 'minimalStock', text: 'Minimale voorraad'},
                {name: 'currentStock', text: 'Huidige voorraad'},
            ],
            kleding: [
                {name: 'color', text: 'Kleur'},
                {name: 'size', text: 'Maat'},
            ],
            tierlantijn: [
                {name: 'weight', text: 'Gewicht'},
            ],
            decoratie: [
                {name: 'size', text: 'Grootte in cm'},
                {name: 'color', text: 'Kleur'},
                {name: 'count', text: 'Aantal in verpakking'},
            ],
        };
    }

    draw() {
        this.count = 1;
        this.maxCount = this.controller.getWarehouseActiveSectionName() === 'Tierlantijn' ? 7 : 8;
        this.productProps = {
            type: this.controller.getWarehouseActiveSectionName(),
            name: null,
            description: null,
            purchasePrice: null,
            sellingPrice: null,
            minimalStock: null,
            currentStock: null,
        };
        this.drawForm();
    }

    drawForm() {
        this.form = document.createElement('form');
        this.drawCurrentStep();
        this.form.appendChild(this.drawButtons());
        this.template.innerHTML = '';
        this.template.appendChild(this.form);
    }

    drawCurrentStep() {
        if (this.count <= 6) {
            let input = this.inputs.all[this.count - 1];
            this.activeInput = input;
            if (input.name === 'sellingPrice') this.form.appendChild(this.drawSellingPriceInput(input.name, input.text));
            else this.form.appendChild(this.drawInput(input.name, input.text));
        } else {
            let input;
            switch (this.productProps.type) {
                case 'Kleding':
                    input = this.inputs.kleding[this.count - 7];
                    this.form.appendChild(this.drawInput(input.name, input.text));
                    break;
                case 'Tierlantijn':
                    input = this.inputs.tierlantijn[this.count - 7];
                    this.form.appendChild(this.drawInput(input.name, input.text));
                    break;
                case 'Decoratie':
                    input = this.inputs.decoratie[this.count - 7];
                    this.form.appendChild(this.drawInput(input.name, input.text));
                    break;
                default:
            }
            this.activeInput = input;
        }
    }

    actionShowNextField() {
        if (this.count !== this.maxCount) {
            this.productProps[this.activeInput.name] = document.getElementById(this.activeInput.name).value;
            this.count++;
            this.drawForm();
        }
    }

    drawInput(name, text) {
        let label = document.createElement('label');
        label.htmlFor = name;
        label.innerText = text;
        let input = document.createElement('input');
        input.id = name;
        input.name = name;
        input.className = 'form-control';
        input.type = 'text';
        let column = document.createElement('div');
        column.className = 'col-md-12';
        column.appendChild(label);
        column.appendChild(input);
        let formGroup = document.createElement('div');
        formGroup.className = 'form-group row';
        formGroup.appendChild(column);
        return formGroup;
    }

    drawSellingPriceInput(name, text) {
        let labelBtw = document.createElement('label');
        labelBtw.htmlFor = name;
        labelBtw.innerText = 'Verkoopprijs inc btw';
        labelBtw.className = 'mt-3'
        let inputBtw = document.createElement('input');
        inputBtw.id = name;
        inputBtw.name = name;
        inputBtw.disabled = true;
        inputBtw.className = 'form-control';
        inputBtw.type = 'text';
        let label = document.createElement('label');
        label.htmlFor = name + 'ex';
        label.innerText = text;
        let input = document.createElement('input');
        input.id = name + 'ex';
        input.name = name + 'ex';
        input.className = 'form-control';
        input.type = 'text';
        input.addEventListener('change', () => {
            let v = parseFloat(input.value)
            v = v * 1.21;
            inputBtw.value = v;
        })
        let column = document.createElement('div');
        column.className = 'col-md-12';
        column.appendChild(label);
        column.appendChild(input);
        column.appendChild(labelBtw);
        column.appendChild(inputBtw);
        let formGroup = document.createElement('div');
        formGroup.className = 'form-group row';
        formGroup.appendChild(column);
        return formGroup;
    }

    drawButtons() {
        let button1 = this.drawNextButton();
        let button2 = this.drawSubmitButton();
        let column = document.createElement('div');
        column.className = 'col-md-12';
        column.appendChild(button1);
        column.appendChild(button2);
        let formGroup = document.createElement('div');
        formGroup.className = 'form-group row';
        formGroup.appendChild(column);
        return formGroup;
    }

    drawNextButton() {
        let button = document.createElement('button');
        button.type = 'submit'
        button.className = this.count === this.maxCount ? 'btn btn-secondary mr-2' : 'btn btn-primary mr-2';
        button.disabled = this.count === this.maxCount;
        button.innerText = 'Volgende';
        button.addEventListener('click', (e) => {
            e.preventDefault();
            this.actionShowNextField();
        });
        return button;
    }

    drawSubmitButton() {
        let button = document.createElement('button');
        button.type = 'submit'
        button.className = this.count !== this.maxCount ? 'btn btn-secondary mr-2' : 'btn btn-primary mr-2';
        button.disabled = this.count !== this.maxCount;
        button.innerText = 'Product aanmaken';
        button.addEventListener('click', (e) => {
            e.preventDefault();
            this.actionSubmit();
        });
        return button;
    }

    actionSubmit() {
        if (this.count === this.maxCount) {
            this.productProps[this.activeInput.name] = document.getElementById(this.activeInput.name).value;
            this.controller.actionCreateNewProduct(this.productProps);
        }
    }
}