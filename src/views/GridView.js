export default class GridView {
    constructor(controller) {
        this.controller = controller;
        this.template = document.getElementById('sectionMain');
    }

    draw(format, products) {
        this.template.innerHTML = '';
        let grid = document.createElement('div');
        grid.style.display = 'grid';
        grid.style.height = '510px';
        grid.style.gridGap = '0em';
        grid.style.gridTemplateRows = 'repeat(var(--grid-rows), 1fr)';
        grid.style.gridTemplateColumns = 'repeat(var(--grid-cols), 1fr)';
        grid.style.setProperty('--grid-rows', 15);
        grid.style.setProperty('--grid-cols', 15);
        for (let i = 0; i < 15; i++) {
            for (let o = 0; o < 15; o++) {
                let cell = document.createElement('div');
                if (format[i][o].valueOf() === '@') {
                    cell.style.backgroundColor = '#ff5e00';
                } else if (format[i][o].valueOf() === '|') {
                    cell.style.backgroundColor = 'darkgray';
                } else if (format[i][o].valueOf() === '-') {
                    cell.style.backgroundColor = 'lightgray';
                    cell.addEventListener('dragover', e => {
                        this.actionDragOver(e);
                    });
                    cell.addEventListener('dragleave', e => {
                        this.actionDragLeave(e);
                    });
                    cell.addEventListener('drop', e => {
                        this.actionDragDrop(e);
                    });
                } else {
                    cell.style.backgroundColor = 'rgb(251,14,14)';
                    let name = format[i][o].valueOf();
                    let product = products.find(product => {
                        return product.name === name;
                    });
                    cell.addEventListener('click', e => {
                        this.actionClickProduct(e, product);
                    })
                    cell.innerText = product.name;
                }
                cell.setAttribute('x', o);
                cell.setAttribute('y', i);
                grid.appendChild(cell);
            }
        }
        this.template.appendChild(grid);
    }

    actionDragOver(e) {
        e.preventDefault();
        e.currentTarget.style.backgroundColor = 'rgb(255, 0, 149)';
    }

    actionDragLeave(e) {
        e.preventDefault();
        e.target.style.backgroundColor = "lightgrey";
    }

    actionDragDrop(e) {
        e.preventDefault();
        e.currentTarget.style.backgroundColor = "red";
        let cord = [e.target.getAttribute('x'), e.target.getAttribute('y')];
        let id = e.dataTransfer.getData('text');
        e.dataTransfer.clearData();
        this.controller.actionPlaceProductOnCell(id, cord);
    }

    actionClickProduct(e, product) {
        e.preventDefault();
        this.controller.viewProductInformation(product);
    }
}