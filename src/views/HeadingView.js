export default class HeadingView {
    constructor(controller) {
        this.controller = controller;
        this.template = document.getElementById('heading');
    }

    draw(activeSectionName) {
        this.template.innerHTML = '<h2 class="text-center">' + activeSectionName + '</h2>';
    }
}