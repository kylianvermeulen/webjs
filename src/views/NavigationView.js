export default class NavigationView {
    constructor(controller) {
        this.controller = controller;
        this.template = document.getElementById('navigation');
    }

    draw(sections, activeSection, createProduct) {
        let template = '';
        sections.forEach(element => {
            if (activeSection != null && element.id === activeSection.id) {
                template += '<li class="nav-item px-3" role="presentation"><a id="nav-item-' + element.name + '" class="nav-link active" href="#">' + element.name + '</a></li>';
            } else {
                template += '<li class="nav-item px-3" role="presentation"><a id="nav-item-' + element.name + '" class="nav-link" href="#">' + element.name + '</a></li>';
            }
        });
        if (createProduct) template += '<li class="nav-item px-3" role="presentation"><a id="nav-item-create" class="nav-link active" href="#">Product aanmaken</a></li>';
        else template += '<li class="nav-item px-3" role="presentation"><a id="nav-item-create" class="nav-link" href="#">Product aanmaken</a></li>';
        this.template.innerHTML = template;
        sections.forEach(element => {
            let navItem = document.querySelector('#nav-item-' + element.name);
            navItem.addEventListener('click', () => {
                this.actionChangeActiveSection(element.name);
            });
        });
        let navItemCreateProduct = document.getElementById('nav-item-create');
        navItemCreateProduct.addEventListener('click', () => {
            this.actionCreateProduct();
        });
    }

    actionChangeActiveSection(name) {
        this.controller.actionChangeWarehouseActiveSection(name);
    }

    actionCreateProduct() {
        this.controller.actionCreateProduct();
    }
}