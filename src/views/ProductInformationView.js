export default class ProductInformationView {
    constructor(controller) {
        this.controller = controller;
        this.template = document.getElementById('sectionMain');
        this.props = {
            all: [
                {name: 'description', text: 'Beschrijving'},
                {name: 'purchasePrice', text: 'Inkoopprijs'},
                {name: 'sellingPrice', text: 'Verkoopprijs inc btw'},
                {name: 'minimalStock', text: 'Minimale voorraad'},
                {name: 'currentStock', text: 'Huidige voorraad'},
            ],
            kleding: [
                {name: 'color', text: 'Kleur'},
                {name: 'size', text: 'Maat'},
            ],
            tierlantijn: [
                {name: 'weight', text: 'Gewicht'},
            ],
            decoratie: [
                {name: 'size', text: 'Grootte in cm'},
                {name: 'color', text: 'Kleur'},
                {name: 'count', text: 'Aantal in verpakking'},
            ],
        };
    }

    draw(product, sectionName) {
        this.canvas = null;
        this.prevX = 0;
        this.currX = 0;
        this.prevY = 0;
        this.currY = 0;
        this.drawActive = false;
        this.template.innerHTML = '';
        let title = document.createElement('h2');
        title.innerText = product.name;
        title.className = 'col-12';
        let closeBtn = document.createElement('button');
        closeBtn.className = 'btn btn-primary my-2 mr-2';
        closeBtn.innerText = 'Verplaats terug naar lijst';
        closeBtn.addEventListener('click', e => {
           e.preventDefault();
           this.actionRemoveFromGrid(product);
        });
        let delBtn = document.createElement('button');
        delBtn.className = 'btn btn-danger my-2 mr-2';
        delBtn.innerText = 'Verwijder uit magazijn';
        delBtn.addEventListener('click', e => {
            e.preventDefault();
            this.actionDeleteProduct(product);
        });
        let props = document.createElement('div');
        this.props.all.forEach(data => {
            props.appendChild(this.drawProp(data.text, product[data.name]));
        });
        switch (sectionName) {
            case 'Kleding':
                this.props.kleding.forEach(data => {
                    props.appendChild(this.drawProp(data.text, product[data.name]));
                });
                break;
            case 'Tielantijn':
                this.props.tierlantijn.forEach(data => {
                    props.appendChild(this.drawProp(data.text, product[data.name]));
                });
                break;
            case 'Decoratie':
                this.props.decoratie.forEach(data => {
                    props.appendChild(this.drawProp(data.text, product[data.name]));
                });
                break;
            default:
                break;
        }
        let input = document.createElement('input');
        input.id = 'image';
        input.type = 'file';
        input.className = 'col-12';
        input.addEventListener('change', e => {
            this.actionUploadImage(e, product);
        });
        let row = document.createElement('div');
        this.canvas = this.drawCanvas(product);
        row.className = 'row';
        row.appendChild(title);
        row.appendChild(closeBtn);
        row.appendChild(delBtn);
        row.appendChild(props);
        row.appendChild(input);
        row.appendChild(this.canvas);
        this.template.appendChild(row);
    }

    actionUploadImage(e, product) {
        e.preventDefault();
        let file = e.target.files[0];
        let fReader = new FileReader();
        let self = this;
        fReader.onload = (function () {
            return function (e) {
                self.canvas.getContext("2d").clearRect(0, 0, self.canvas.width, self.canvas.height);
                let buffer = new Image();
                buffer.src = e.target.result;
                buffer.onload = function () {
                    let sizer = Math.min((340 / buffer.width), (400 / buffer.height));
                    self.canvas.width = 340;
                    self.canvas.height = 400;
                    self.canvas.getContext("2d").drawImage(buffer, 0, 0, buffer.width, buffer.height, 0, 0, buffer.width * sizer, buffer.height * sizer);
                    self.controller.actionUploadImageToProduct(product, self.canvas.toDataURL('image/png'));
                }
            }
        })(file);
        fReader.readAsDataURL(file);
    }

    drawCanvas(product) {
        let canvas = document.createElement('canvas');
        canvas.id = 'canvas';
        canvas.className = 'col-12 mt-3 border border-primary p-0';
        if (product.image != null) {
            let img = new Image();
            img.onload = function () {
                canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height);
                let sizer = Math.min((340 / img.width), (400 / img.height));
                canvas.width = 340;
                canvas.height = 400;
                canvas.getContext("2d").drawImage(img, 0, 0, img.width, img.height, 0, 0, img.width * sizer, img.height * sizer);
            };
            img.src = product.image;
        }
        canvas.addEventListener('mousemove', e => {
            if (this.drawActive) {
                this.prevX = this.currX;
                this.prevY = this.currY;
                this.currX = e.clientX - canvas.getBoundingClientRect().left;
                this.currY = e.clientY - canvas.getBoundingClientRect().top;
                this.actionDrawPoint(canvas.getContext('2d'));
            }
        });
        canvas.addEventListener('mousedown', e => {
            this.prevX = this.currX;
            this.prevY = this.currY;
            this.currX = e.clientX - canvas.getBoundingClientRect().left;
            this.currY = e.clientY - canvas.getBoundingClientRect().top;
            this.drawActive = true;
        });
        canvas.addEventListener('mouseup', e => {
            this.drawActive = false;
            this.currX = 0;
            this.currY = 0;
            this.prevX = 0;
            this.prevY = 0;
            this.controller.actionUploadImageToProduct(product, this.canvas.toDataURL('image/png'));
        });
        return canvas;
    }

    actionDrawPoint(ctx) {
        ctx.beginPath();
        ctx.moveTo(this.prevX, this.prevY);
        ctx.lineTo(this.currX, this.currY);
        ctx.strokeStyle = 'red';
        ctx.lineWidth = 3;
        ctx.stroke();
        ctx.closePath();
    }

    drawProp(text, value) {
        let span = document.createElement('label');
        span.innerText = text + ': ' + value;
        span.className = 'col-12';
        return span;
    }

    actionRemoveFromGrid(product) {
        this.controller.actionRemoveProductFromGrid(product);
    }

    actionDeleteProduct(product) {
        this.controller.actionDeleteProduct(product);
    }
}