export default class ProductsView {
    constructor(controller) {
        this.controller = controller
        this.template = document.getElementById('sectionSidebarProducts');
    }

    draw(products) {
        let title = '<h3 class="text-center" style="color: black">Producten</h3>';
        let div = document.createElement('div');
        div.className = 'd-flex flex-column justify-content-center align-content-center p-4';
        div.style = 'background-color: lightgray'
        div.innerHTML += title
        products.forEach(product => {
            div.appendChild(this.drawProduct(product));
        })
        this.template.appendChild(div);
    }

    drawProduct(product) {
        let productDiv = document.createElement('div');
        let name = '<span style="color: black; user-select: none;">' + product.name + '</span>'
        productDiv.innerHTML += name;
        productDiv.id = product.name;
        productDiv.draggable = true;
        productDiv.addEventListener('dragstart', e => {
            this.actionStartDrag(e);
        });
        return productDiv;
    }

    actionStartDrag(e) {
        e.dataTransfer.dropEffect = 'move';
        e.dataTransfer.setData('text/plain', e.target.id)
    }
}