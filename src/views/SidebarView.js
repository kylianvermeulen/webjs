import WeatherView from "./WeatherView";
import ProductsView from "./ProductsView";

export default class SidebarView {
    constructor(controller) {
        this.controller = controller;
        this.template = document.getElementById('sectionSidebar');
    }

    draw(locationData, products) {
        this.template.innerHTML = '';
        this.template.innerHTML += '<div id="sectionSidebarWeather"></div>';
        this.template.innerHTML += '<div id="sectionSidebarProducts"></div>';
        this.drawWeatherView(locationData);
        this.drawProductsView(products);
    }

    drawWeatherView(locationData) {
        this.weatherView = new WeatherView(this.controller).draw(locationData);
    }

    drawProductsView(products) {
        this.productsView = new ProductsView(this.controller).draw(products);
    }
}