export default class WeatherView {
    constructor(controller) {
        this.controller = controller;
        this.template = document.getElementById('sectionSidebarWeather');
    }

    draw(data) {
        data.then(data => {
            this.template.innerHTML = '<div class="d-flex flex-column justify-content-center align-content-center p-4" style="background: rgb(251,14,14); background: linear-gradient(90deg, rgba(251,14,14,1) 0%, rgba(255,0,149,1) 100%);">' +
                '<img src="resources/images/' + data.weather[0].icon + '.png" class="align-self-center" style="width: 80px; height: 80px;" alt="icon"/>' +
                '<h4 class="text-center" style="font-size: 2.2em;">' + Math.floor(data.main.temp - 273) + '°<span style="font-size: 0.5em">C</span></h4>' +
                '<h4 class="text-center">' + data.name + '</h4>' +
                '<input id="location" class="form-control my-2" type="text" name="location" placeholder="Gewenste locatie">' +
                '</div>';
            let locationInput = document.getElementById('location');
            locationInput.addEventListener('change', () => {
                this.actionUpdateLocation(locationInput.value);
            });
            return data;
        }).then(data => {
            this.actionStoreLocationInLocalStorage(data);
        }).catch(err => {
            this.template.innerHTML = '<div class="d-flex flex-column justify-content-center align-content-center p-4" style="background: rgb(251,14,14); background: linear-gradient(90deg, rgba(251,14,14,1) 0%, rgba(255,0,149,1) 100%);">' +
                '<h4 class="text-center">Locatie niet gevonden</h4>' +
                '<input id="location" class="form-control my-2" type="text" name="location" placeholder="Gewenste locatie">' +
                '</div>'
            let locationInput = document.getElementById('location');
            locationInput.addEventListener('change', () => {
                this.actionUpdateLocation(locationInput.value);
            });
        });
    }

    actionUpdateLocation(location) {
        this.draw(this.controller.getWeatherDataByQuery(location));
    }

    actionStoreLocationInLocalStorage(data) {
        this.controller.actionStoreLocationInLocalStorage(data);
    }
}